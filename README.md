## HyperHDR User Service Installer

This script assists in setting up HyperHDR to run as a systemd user service. It provides an alternative to running HyperHDR as a root service, offering more flexibility and security.

### Features:

*    Checks if hyperhdr is installed. If not, it will guide you through the installation process.
*    Offers an option to disable the default root service (hyperhdr@pi.service) and activate the user service.
*    Sets up a systemd user service for HyperHDR.
*    Waits for the user's session to be fully initialized (specifically waits for lxsession) and then introduces an additional 8-second delay before starting HyperHDR.This ensures that the display capture is ready.

### Usage:

To use the script, you can either download it and run it locally or execute it directly from the repository using:

bash
```
sh -c "$(curl -sL https://gitlab.com/gllmar/sysd-hyperhdr/-/raw/main/install_hyperhdr_user_service.sh)"

```

Follow the on-screen prompts to complete the setup.

## Managing the HyperHDR User Service:

Once the user service is installed, you can manage it using the systemctl --user command. Here are some examples:

### Start the service:


bash
```
systemctl --user start hyperhdr.service
```

### Stop the service:

bash
```
systemctl --user stop hyperhdr.service
```


### Restart the service:

bash
```
systemctl --user restart hyperhdr.service
```

### Check the status of the service:

bash
```
systemctl --user status hyperhdr.service
```

### Enable the service to start on boot:
```
bash

systemctl --user enable hyperhdr.service
```

### Disable the service from starting on boot:

bash
```
 systemctl --user disable hyperhdr.service
```

Remember, since this is a user service, you don't need sudo for these commands.
