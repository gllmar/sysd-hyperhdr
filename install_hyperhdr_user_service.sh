#!/bin/bash

# Check if hyperhdr exists in /usr/bin/hyperhdr
if [[ ! -f /usr/bin/hyperhdr ]]; then
    echo "hyperhdr not found. Installing..."

    # Detect architecture
    ARCH=$(uname -m)
    DEB_FILE=""
    case $ARCH in
        armv6l)
            DEB_FILE="HyperHDR-19.0.0.0-Linux-armv6l.deb"
            ;;
        aarch64)
            DEB_FILE="HyperHDR-19.0.0.0-Linux-aarch64.deb"
            ;;
        x86_64)
            DEB_FILE="HyperHDR-19.0.0.0-Linux-x86_64.deb"
            ;;
        *)
            echo "Unsupported architecture: $ARCH"
            exit 1
            ;;
    esac

    # Download the appropriate .deb package
    wget "https://github.com/awawa-dev/HyperHDR/releases/download/v19.0.0.0/$DEB_FILE"

    # Install the .deb package
    sudo apt install ./$DEB_FILE

    # Prompt user to remove the .deb package
    read -p "Do you want to remove the downloaded .deb package? (y/n) " remove_choice
    if [[ $remove_choice == "y" || $remove_choice == "Y" ]]; then
        rm $DEB_FILE
        echo "Removed $DEB_FILE."
    fi
else
    echo "hyperhdr is already installed."
fi

# Ask the user if they want to disable the root service and enable the user service
read -p "Do you want to disable the root service (hyperhdr@pi.service) and activate the user service? (y/n) " service_choice
if [[ $service_choice == "y" || $service_choice == "Y" ]]; then
    sudo systemctl stop hyperhdr@pi.service
    sudo systemctl disable hyperhdr@pi.service
    echo "Root service hyperhdr@pi.service has been stopped and disabled."
fi

# Define the path where the service file will be saved
SERVICE_PATH="$HOME/.config/systemd/user/hyperhdr.service"

# Create the systemd user directory if it doesn't exist
mkdir -p ~/.config/systemd/user/

# Generate the content of the hyperhdr.service file
cat <<EOL > $SERVICE_PATH
[Unit]
Description=HyperHDR User Service

[Service]
Type=simple
ExecStartPre=/bin/bash -c 'until pgrep lxsession; do sleep 1; done; sleep 4'
ExecStart=/usr/bin/env DISPLAY=:0 /usr/bin/hyperhdr
Restart=always
RestartSec=5

[Install]
WantedBy=default.target
EOL

# Reload the systemd user manager to recognize the new service
systemctl --user daemon-reload
systemctl --user enable hyperhdr.service
echo "HyperHDR user service file has been installed."

# Prompt the user to restart the user service
read -p "Do you want to restart the HyperHDR user service now? (y/n) " restart_choice
if [[ $restart_choice == "y" || $restart_choice == "Y" ]]; then
    systemctl --user restart hyperhdr.service
    echo "HyperHDR user service has been restarted."
fi